name := "markets-update"

version := "0.1"
scalaVersion := "2.12.8"

val http4sVersion         = "0.20.1"

val catsVersion           = "1.6.0"
val scalaCheckVersion     = "1.14.0"
val scalaTestVersion      = "3.0.7"
val circeVersion          = "0.11.1"
val circeConfigVersion    = "0.6.1"
val pureConfigVersion     = "0.11.0"
val logbackVersion        = "1.2.3"
val sangriaVersion        = "1.4.2"

libraryDependencies ++= Seq(
  "org.typelevel"         %% "cats-core"              % catsVersion,

  "io.circe"              %% "circe-generic"          % circeVersion,
  "io.circe"              %% "circe-literal"          % circeVersion,
  //"io.circe"              %% "circe-generic-extras"   % CirceVersion,
  "io.circe"              %% "circe-parser"           % circeVersion,
  //"io.circe"              %% "circe-java8"            % CirceVersion,
  "io.circe"              %% "circe-config"           % circeConfigVersion,

  "org.http4s"            %% "http4s-blaze-server"    % http4sVersion,
  "org.http4s"            %% "http4s-circe"           % http4sVersion,
  "org.http4s"            %% "http4s-dsl"             % http4sVersion,
  "org.http4s"            %% "http4s-blaze-client"    % http4sVersion,

  "com.github.pureconfig" %% "pureconfig"             % pureConfigVersion,
  "com.github.pureconfig" %% "pureconfig-http4s"      % pureConfigVersion,

  "ch.qos.logback"        %  "logback-classic"        % logbackVersion,

  "org.scalactic"         %% "scalactic"              % scalaTestVersion  % Test,
  "org.scalacheck"        %% "scalacheck"             % scalaCheckVersion % Test,
  "org.scalatest"         %% "scalatest"              % scalaTestVersion  % Test,
)

scalacOptions ++= Seq(
  "-Ypartial-unification",
  "-language:higherKinds"
)
