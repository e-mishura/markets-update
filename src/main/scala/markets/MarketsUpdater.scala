package markets


import scala.util.Random
import cats.effect.Sync
import fs2._
import markets.domain._

import scala.math.BigDecimal.RoundingMode


trait MarketsUpdater[F[_]]{
  def updates: Stream[F, Market]
}

class InMemoryMarketUpdater[F[_]: Sync](nMarkets: Int) extends MarketsUpdater[F] {
  override def updates: Stream[F, Market] = Stream.eval(randomMarket).repeat

  private def randomMarket: F[Market] = Sync[F].delay({

    val nm = Random.nextInt(nMarkets) + 1
    val px = BigDecimal.valueOf(Random.nextDouble()).setScale(3, RoundingMode.HALF_UP)
    Market("Market" + nm, px)

  })
}
