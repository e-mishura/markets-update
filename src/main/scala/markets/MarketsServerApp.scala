package markets

import scala.concurrent.duration._

import cats.effect._
import cats.syntax.all._
import fs2._


import pureconfig.loadConfig
import pureconfig.generic.auto._
import pureconfig.module.http4s._


import markets.StreamUtil._

import markets.config._



object MarketsServerApp extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {

//    val srv = for {
//      cfg <- loadConfig[EndPoint]("server").toIO
//    } yield ???

    val mu = new InMemoryMarketUpdater[IO](10)

    val mus = marketUpdateHb.zipRight(mu.updates).log("market update")

    run(mus)
    //IO.pure(ExitCode.Success)
  }

  def marketUpdateHb: Stream[IO, Unit] = Stream.fixedRate(0.1.second).interruptAfter(10.seconds)

  def run(stream: Stream[IO, _]): IO[ExitCode] = stream.compile.drain.as(ExitCode.Success)

}
