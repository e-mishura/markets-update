package markets.domain

case class Market(name: String, price: BigDecimal)
