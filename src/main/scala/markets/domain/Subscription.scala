package markets.domain

sealed trait Subscription

case class Subscribe(markets: Seq[String]) extends Subscription
case class Unsubscribe(markets: Seq[String]) extends Subscription
case object Disconnect extends Subscription
