package markets

import cats.effect.{Concurrent, IO}
import cats.effect.implicits._
import cats.syntax.all._
import fs2.Stream
import fs2.concurrent.Queue

object StreamUtil {

  implicit class StreamLogger[F[x] <: IO[x], A](stream: Stream[F, A]) {
    def log(prefix: String): Stream[IO, A] =
      stream.evalMap( a =>
          IO { println(s"$prefix >> $a") } >>
          IO {a}
        )
  }

  implicit class StreamAsync[F[_], A](stream: Stream[F, A])(implicit F: Concurrent[F]) {
    def async: Stream[F, A] = for {
      q <- Stream.eval(Queue.bounded[F, Option[A]](1))
      in = stream.noneTerminate.through(q.enqueue)
      out = q.dequeue.unNoneTerminate
      a <- out concurrently in
    } yield a
  }

}
