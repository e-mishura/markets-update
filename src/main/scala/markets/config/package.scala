package markets

import cats.implicits._
import cats.effect.IO
import pureconfig.ConfigReader
import pureconfig.error.ConfigReaderException

import scala.reflect.ClassTag

package object config {

  implicit class ConfigResult2IO[A: ClassTag](result: ConfigReader.Result[A]) {
    def toIO: IO[A] = IO.fromEither(
      result.leftMap(new ConfigReaderException[A](_))
    )
  }

}
