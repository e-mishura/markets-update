package markets.config

import org.http4s.Uri

case class EndPoint(host: Uri, port: Int)
