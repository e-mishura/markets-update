package markets

import java.time.{LocalDateTime, ZoneOffset}
import java.util.concurrent.atomic.AtomicLong

import cats.effect.Concurrent
import cats.effect.concurrent.Ref
import cats.implicits._

import fs2._
import fs2.concurrent.Queue

import markets.domain._


case class ClientState(
      id: Long,
      nUpdates: Long = 0,
      markets: Map[String, BigDecimal] = Map(),
      created: LocalDateTime = LocalDateTime.now(ZoneOffset.UTC)) {
  def updated(m: Market): ClientState = copy(
    nUpdates = nUpdates + 1,
    markets = markets.updated(m.name, m.price)
  )
}

case class ClientStats(id: Long, nUpdates: Long, created: LocalDateTime, terminated: LocalDateTime)

object ClientStats {
  def apply(state: ClientState): ClientStats = ClientStats(
    state.id,
    state.nUpdates,
    state.created,
    terminated = LocalDateTime.now(ZoneOffset.UTC)
  )
}


trait Client[F[_]] {
  def id: Long
  def state: F[ClientState]
  def subscribe(markets: Seq[String]): F[Unit]
  def unsubscribe(markets: Seq[String]): F[Unit]
  def connect: Stream[F, Unit]
  def disconnect: F[ClientStats]
}


class InMemoryClient[F[_]: Concurrent](
  val id: Long,
  toClient : Stream[F, Market],
  fromClient: Pipe[F, Subscription, Unit],
  stateRef: Ref[F, ClientState],
  subscription: Queue[F, Subscription]) extends Client[F] {

  def connect: Stream[F, Unit] = {
    val tc = toClient.through(processUpdate)
    val fc = subscription.dequeue.through(fromClient)
    tc concurrently tc
  }

  private def processUpdate: Pipe[F, Market, Unit] = _.map(
    mkt => stateRef.modify(
        state => (state.updated(mkt), Unit)
    )
  )

  override def disconnect: F[ClientStats] = for {
    _         <- subscription.enqueue1(Disconnect)
    state     <- stateRef.get
  } yield ClientStats(state)

  override def subscribe(markets: Seq[String]): F[Unit] =
    subscription.enqueue1(Subscribe(markets))

  override def unsubscribe(markets: Seq[String]): F[Unit] =
    subscription.enqueue1(Unsubscribe(markets))



  def state: F[ClientState] = stateRef.get
}

object InMemoryClient {

  val clientId = new AtomicLong(0L)

  def apply[F[_]](
     toClient : Stream[F, Market],
     fromClient: Pipe[F, Subscription, Unit])(implicit F: Concurrent[F]): F[Client[F]] = for {

    id            <- F.pure(clientId.incrementAndGet())
    stateRef      <- Ref.of[F, ClientState](ClientState(id))
    subscription  <- Queue.bounded[F, Subscription](10)

  } yield new InMemoryClient(id, toClient, fromClient, stateRef, subscription)
}
