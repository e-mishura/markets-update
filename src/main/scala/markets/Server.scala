package markets

import cats.effect.{Concurrent, Sync}
import cats.effect.implicits._
import cats.implicits._
import fs2.Stream
import fs2.concurrent.{Queue, SignallingRef}
import markets.domain.Market


case class SubscriberEntry[F[_]](id: Long, queue: Queue[F, Market])
case class SubscriberDescriptor[F[_]](
             subscriber: SubscriberEntry[F],
             interrupt: F[SignallingRef[F, Boolean]],
             markets: Set[String] = Set()) {
  def removeMarkets(mkts: List[String]): SubscriberDescriptor[F] =
    copy(markets = markets -- mkts)
  def addMarkets(mkts: List[String]): (SubscriberDescriptor[F], List[String]) = {
    val mset = mkts.toSet
    val newMarkets = mset -- markets
    (copy(markets = markets ++ newMarkets), newMarkets.toList)
  }
}
case class MarketEntry[F[_]](market: String, last: Option[Market], subscribers: List[(Long, Queue[F, Market])] = Nil) {
  def removeSubscriber(id: Long): MarketEntry[F]
    = copy(subscribers = subscribers.filter{ case (sid, _) => sid != id })
}


case class ServerState[F[_]](
                              subscribers: Map[Long, SubscriberDescriptor[F]] = Map.empty[Long, SubscriberDescriptor[F]],
                              marketSubs: Map[String, MarketEntry[F]] = Map.empty[String, MarketEntry[F]])
                            (implicit F : Sync[F]) {

  def update(mkt: Market): F[ServerState[F]] = {

    val me = marketSubs.get(mkt.name) match {
      case None => MarketEntry[F](mkt.name, Some(mkt))
      case Some(me) => me.copy(last = Some(mkt))
    }

    val broadcastF = me.subscribers.traverse[F, Unit] {case (_, q) => q.enqueue1 (mkt)}
    broadcastF >> F.delay{ copy(marketSubs = marketSubs.updated(mkt.name, me)) }
  }

  def connectClient(id: Long, queue: Queue[F, Market], interrupt: F[SignallingRef[F, Boolean]]): F[ServerState[F]] = {
    val entry = SubscriberEntry(id, queue)
    val descriptor = SubscriberDescriptor(entry, interrupt)
    F.delay { copy(subscribers = subscribers.updated(id, descriptor)) }
  }

  def disconnectClient(id: Long): F[ServerState[F]] = for {
    d <-  F.delay { subscribers.get(id) }
    s <- d match {
      case None    => F.pure(this)
      case Some(d) => for {
        i <- d.interrupt
        _ <- i.set(true)
        s <- F.delay { unsubscribeClient(id, d.markets.toList) }
            .map(_.copy(subscribers = subscribers - id))
      } yield s
    }
  } yield s

  def subscribeClient(id: Long, markets: List[String]): ServerState[F] = {
    val subDescr = subscribers
      .get(id)
      .map(_.addMarkets(markets))
    subDescr match {
      case None => this
      case Some((subDescr, markets)) =>

        val updatedSubscribers = subscribers.updated(id, subDescr)

        val updatedMarketSubs = markets
          .view
          .map(m => marketSubs.getOrElse(m, MarketEntry[F](m, None)))
          .map(me => me.copy(subscribers = (id, subDescr.subscriber.queue) :: me.subscribers))
          .foldLeft(marketSubs) { (subs, me) => subs.updated(me.market, me) }

        ServerState[F](updatedSubscribers, updatedMarketSubs)
    }
  }

  def unsubscribeClient(id: Long, markets: List[String]): ServerState[F] =  {
    val subDescr = subscribers
      .get(id)
      .map(_.removeMarkets(markets))
     subDescr match {
      case None       => this
      case Some(subDescr)  =>

        val updatedSubscribers = subscribers.updated(id, subDescr)

        val updatedMarketSubs = markets
          .view
          .flatMap(m => marketSubs.get(m))
          .map(_.removeSubscriber(id))
          .foldLeft(marketSubs) { (subs, me) => subs.updated(me.market, me) }

        ServerState[F](updatedSubscribers, updatedMarketSubs)
    }
  }
}

class Server[F[_]](implicit F: Concurrent[F]) {


  def bootstrap(updates: Stream[F, Market]): Stream[F, ServerState[F]] = for {
    inQ       <- Stream.eval(Queue.bounded[F, Market](100))
    seed      =  ServerState[F]()
    _         <- Stream.eval(updates.through(inQ.enqueue).compile.drain.start)
    outServer <- inQ.dequeue.evalScan(seed) { (state, market) => state.update(market) }
  } yield outServer


}
